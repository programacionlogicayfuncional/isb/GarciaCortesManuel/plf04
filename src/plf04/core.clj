(ns plf04.core)

(defn string-e-1
  [st]
  (letfn [(f [ts]
            (if (or (empty? ts)(> (count (get (group-by identity ts) \e)) 3))
              false
              (if (== (int (first ts)) 101)
                true
                (f (rest ts)))))]
    (f st)))

(string-e-1 "hello")
(string-e-1 "heelle")
(string-e-1 "heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")



(defn string-e-2
  [st]
  (letfn [(f [ts acc]
             (if (or (empty? ts) (> (count (get (group-by identity ts) \e)) 3))
              acc 
               (if (== (int (first ts)) 101)
                 true
                 (f (rest ts) acc) )))]
    (f st false)
  ))

(string-e-2 "hello")
(string-e-2 "heelle")
(string-e-2 "heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")
 

(defn string-Times-1
  [st x]
  (letfn [(f [ts y]
             (if (zero? y)
               (str)
               (str ts (f ts (dec y)))))]
    (f st x)))

(string-Times-1 "hi" 2)
(string-Times-1 "hi" 3)
(string-Times-1 "hi" 1)
(string-Times-1 "hi" 0)
(string-Times-1 "!Oh boy" 2)
(string-Times-1 "x" 4)
(string-Times-1 "", 4)
(string-Times-1 [\c \o \d \e],2 )
(string-Times-1 "code" 3)

(defn string-Times-2
  [st x]
  (letfn [(f [st y acc]
             (if (zero? y)
               acc
               (str st (f st (dec y) acc))))]
    (f st x "")))

(string-Times-2 "hi" 2)
(string-Times-2 "hi" 3)
(string-Times-2 "hi" 1)
(string-Times-2 "hi" 0)
(string-Times-2 "!Oh boy" 2)
(string-Times-2 "x" 4)
(string-Times-2 "", 4)
(string-Times-2 "code",2)
(string-Times-2 "code" 3)

(defn front-Times-1
  [st x]
  (letfn [(f [ts y]
             (if (or (zero? y) (empty? ts))
               (str)
               (str (into [] (take 3 ts)) (f ts (dec y)))))]
    (f st x)))




(front-Times-1 "chocolate" 2)
(front-Times-1 "chocolate" 3)
(front-Times-1 "Abc" 3)
(front-Times-1 "Ab" 4)
(front-Times-1 "A" 4)
(front-Times-1 "" 4)
(front-Times-1 "Abc" 0)

(defn front-Times-2
  [ts x]
  (letfn [(f [st x acc]
             (if (or (empty? st) (zero? x))
               acc
               (str (into [] (take 3 st)) (f ts (dec x) acc))))]
    (f ts x "")))

(front-Times-2 "chocolate" 2)
(front-Times-2 "chocolate" 3)
(front-Times-2 "Abc" 3)
(front-Times-2 "Ab" 4)
(front-Times-2 "A" 4)
(front-Times-2 "" 4)
(front-Times-2 "Abc" 0)

(defn count-xx-1
  [st]
  (letfn [(f [st] 
             (if (or (empty? st) (nil? (get (frequencies st) \x)) ) 
               0
               (if (and (distinct? (first (rest st)) nil) (== (int (first st)) (int (first (rest st)))))
                 (inc (f (rest st)))
                 (f (rest st)))))]
    (f st)))

(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "hexxo thexxe")
(count-xx-1 "")
(count-xx-1 "kittens")
(count-xx-1 "kittensxxx")

(defn count-xx-2
  [st]
  (letfn [(f [st acc]
             (if (or (empty? st) (nil? (get (frequencies st) \x)))
                 acc
               (if (and (distinct? (first (rest st)) nil) (== (int (first st)) (int (first (rest st)))))
                 (f (rest st) (inc acc))
                 (f (rest st) acc))))]
    (f st 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "hexxo thexxe")
(count-xx-2 "")
(count-xx-2 "kittens")
(count-xx-2 "kittensxxx")


(defn string-splosion-1
  [st]
   (letfn [(f [st]
              (if (== (count st) 1)
                st
                (str (f (into [] (drop-last st))) st)))]
     (f st)))

(string-splosion-1 "code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [st]
   (letfn [(f [st acc]
              (if (== (count st) 1)
                acc
                (str (f (into [] (drop-last st)) (into [] (drop-last acc))) st)))]
     (f st st)))

(string-splosion-2 "code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

(defn array-123-1
  [xs]
  (letfn [(f [xs]
             (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs))))) 
               false
               (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                 true
                 (f (rest xs)))))]
    (f xs)))

(array-123-1 [1, 1, 2, 3, 1])
(array-123-1 [1, 1, 2, 4, 1])
(array-123-1 [1, 1, 2, 1, 2, 3])
(array-123-1 [1, 1, 2, 1, 2, 1])
(array-123-1 [1, 2, 3, 1, 2, 3])
(array-123-1 [1, 2, 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [xs]
  (letfn [(f [xs acc] 
             (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
               acc
               (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                 true
                 (f (rest xs) acc))))]
    (f xs false)))

(array-123-2 [1, 1, 2, 3, 1])
(array-123-2 [1, 1, 2, 4, 1])
(array-123-2 [1, 1, 2, 1, 2, 3])
(array-123-2 [1, 1, 2, 1, 2, 1])
(array-123-2 [1, 2, 3, 1, 2, 3])
(array-123-2 [1, 2, 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])

(defn alt-pairs-1
  [st]
  (letfn [(f [st]
             (if (empty? st)
               (str)
               (str (first st) (first (rest st)) (f (rest (rest (rest (rest st))))))))]
    (f st)))

(alt-pairs-1 "kitten")
(alt-pairs-1 "chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

(defn alt-pairs-2
  [st]
  (letfn [(f [st acc]
             (if (empty? st)
               acc
               (str (first st) (first (rest st)) (f (rest (rest (rest (rest st)))) acc))))]
    (f st "")))

(alt-pairs-2 "kitten")
(alt-pairs-2 "chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

(defn string-yak-1
  [st]
  (letfn [(f [st]
             (if (or (empty? st) (nil? (first (rest st))) (nil? (first (rest (rest st)))))
               st
               (if (and (== (int (first st)) 121) (== (int (first (rest st))) 97) (== (int (first (rest (rest st)))) 107))
                 (str (f (rest (rest (rest st)))))
                 ( str (first st) (f (rest st))))))]
    (f st)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [st]
  (letfn [(f [st acc]
             (if (or (empty? st) (nil? (first (rest st))) (nil? (first (rest (rest st)))))
               acc
               (if (and (== (int (first st)) 121) (== (int (first (rest st))) 97) (== (int (first (rest (rest st)))) 107))
                 (str (f (rest (rest (rest st))) (rest (rest (rest acc)))))
                 (str (first st) (f (rest st) (rest acc))))))]
    (f st st)))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")

(defn has-271-1
  [xs]
   (letfn [(f [xs]
              (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
                false
                (if (== (- (first (rest xs)) 5) (first xs))
                  (if (== (+ (first (rest (rest xs))) 1) (first xs))
                    true
                    (if (and (>= (+ (- (first xs) 1) 2) (first (rest (rest xs)))) (<= (- (- (first xs) 1) 2) (first (rest (rest xs)))) )
                      true
                      (f (rest xs))))
                  (f (rest xs)))))]
     (f xs)))


(has-271-1 [1, 2, 7, 1])
(has-271-1 [1, 2 ,8, 1])
(has-271-1 [2, 7, 1])
(has-271-1 [3, 8, 2])
(has-271-1 [2, 7, 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4, 5, 3, 8, 0])
(has-271-1 [2, 7, 5, 10, 4])
(has-271-1 [2, 7, -2, 4, 9, 3])
(has-271-1 [2, 7, 5, 10, 1])
(has-271-1 [2, 7, -2, 4, 10, 2])
(has-271-1 [1, 1, 4, 9, 0])
(has-271-1 [1, 1, 4, 9, 4, 9, 2])

(defn has-271-2
  [xs]
  (letfn [(f [xs acc]
            (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
              acc
              (if (== (- (first (rest xs)) 5) (first xs))
                (if (== (+ (first (rest (rest xs))) 1) (first xs))
                  true
                  (if (and (>= (+ (- (first xs) 1) 2) (first (rest (rest xs)))) (<= (- (- (first xs) 1) 2) (first (rest (rest xs)))))
                    true
                    (f (rest xs) acc)))
                (f (rest xs) acc))))]
    (f xs false)))

(has-271-2 [1, 2, 7, 1])
(has-271-2 [1, 2 ,8, 1])
(has-271-2 [2, 7, 1])
(has-271-2 [3, 8, 2])
(has-271-2 [2, 7, 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4, 5, 3, 8, 0])
(has-271-2 [2, 7, 5, 10, 4])
(has-271-2 [2, 7, -2, 4, 9, 3])
(has-271-2 [2, 7, 5, 10, 1])
(has-271-2 [2, 7, -2, 4, 10, 2])
(has-271-2 [1, 1, 4, 9, 0])
(has-271-2 [1, 1, 4, 9, 4, 9, 2])









































